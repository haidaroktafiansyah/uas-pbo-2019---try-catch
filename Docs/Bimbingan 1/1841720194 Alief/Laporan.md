# Mencari Sumber Air Terdekat Dari Firespot

## Abstract
>Kebakaran adalah musibah yang di sebabkan oleh faktor lingkungan atau mahkluk hidup . untuk meminimalisir terjadinya kebakaran di suatu tempat atau lokasi kita bisa menggunakan algoritma yang mampu mendeteksi asap kebakaran dan ketepatan lokasi terjadinya kebakaran serta mengambil rute terdekat dengan sumber air agar mempercepat proses pemadaman api pada lokasi kebakaran.

## Input

>letak secara akurat Lokasi Kebakaran.

>Pemetaan rute kebakaran.

>Pengambilan rute sumber air terdekat.

## Proses

>lokasi kebakaran diubah menjadi sebuah titik koordinat agar memudahkan pemetaan lokasi kebakaran ke dalam peta, setelah titik koordinat di temukan, dilakukan pemetaan sumber air terdekat kurang lebih 5m , Info dari data tersebut akan di simpan dan di proses menjadi algoritma algoritma yang telah di tentukan dan menghasilkan rute terbaik dan tercepat dari lokasi terjadinya kebakaran.

## output

>Lokasi sumber air beserta rute terbaik dan tercepat ke tempat lokasi kebakaran
>Menampilkan luas wilayah kebakaran sehingga menghasilkan wilayah evakuasi yang aman jauh dari lokasi kebakaran

## Pernyataan Diri

>Saya menyatakan isi tugas, dan penulisan laporan ini dibuat oleh saya sendiri. Saya tidak melakukan plagiasi, kecurangan, menyalin/menggandakan milik orang lain.
Jika saya melakukan plagiasi, kecurangan, atau melanggar hak kekayaan intelektual, saya siap untuk mendapat sanksi atau hukuman sesuai peraturan perundang-undangan yang berlaku.
Ttd,

*** (Alief Al Gaffari) ***