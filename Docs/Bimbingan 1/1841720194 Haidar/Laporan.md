# Mencari Sumber Air Terdekat Dari Firespot

## Abstract

Kebakaran adalah salah satu musibah yang disebabkan oleh manusia yang disengaja ataupun yang tidak sengaja. Kebakaran yang besar dapat melahap benda yang ada disekitarnya dengan cepat sehingga mengalami kerugian yang besar pula bagi yang mengalaminya. Disini akan dibutuhkan jasa pemadam kebakaran untuk mengurangi risiko kebakaran yang akan berkelanjutan jika tidak langsung ditanggapi. Untuk menuju ke lokasi kebakaran, pemadam kebakaran harus dengan cepat agar situasi tidak semakin parah. Masalah pencarian rute perjalanan ini terkadang menjadi sebuah permasalahan yang cukup kompleks. Hal ini disebabkan kondisi jalan yang harus dilewati mobil pemadam kebakaran memiliki tingkat kepadatan kendaraan yang cukup tinggi. Pada penelitian tugas akhir ini, penulis membuat sebuah aplikasi pencarian rute untuk mobil pemadam kebakaran berbasis android dengan menggunakan algoritma Djikstra atupun Floyd-Warshall. Algoritma ini memberikan solusi pencarian rute yang optimal untuk pemadam kebakaran dengan memperhatikan kondisi jalan seperti kemacetan, kondisi ruas jalan dan sumber air terdekat dari lokasi kebakaran. Proses pencarian didasarkan pada perhitungan jarak tiap simpul dan memilih jarak terkecil antar titik. Diharapkan dengan adanya aplikasi ini dapat membantu memberikan panduan rute perjalanan yang optimal sehingga pemadam kebakaran dapat sampai di lokasi kebakaran dengan cepat dan kerugian yang disebabkan oleh kebakaran dapat ditekan seminimal mungkin

## Input

* letak secara akurat Lokasi Kebakaran

## Proses

Tempat kejadian kebakaran akan di konversi menjadi sebuah titik koordinat agar memudahkan pencariannya ke dalam peta, setelah titik koordinat di temukan, pencarian water hydrant dan sumber air akan di lakukan kurang lebih sejauh radius 5KM, daerah tersebut akan di telusuri berdasarkan data yang sudah ada, seluruh data water hydrant atau sumber air terdekat akan di simpan dan di komputasi berdasarkan algoritma-algoritma yang telah di tentukan lalu keseluruhan water hydrant atau sumber air akan di komparasikan untuk mencari waterhydrant terdekat mencari rute terbaik dari tempat terjadinya kebakaran

## output

* Lokasi water hydrant beserta jalur tercepat yang bisa di lewati mobil kebakaran