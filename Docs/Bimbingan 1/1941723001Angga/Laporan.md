# Mencari Sumber Air Terdekat Dari Firespot


## Abstract

        Kebakaran merupakan suatu bencana yang disebabkan oleh api yang bergerak bebas dan dapat menyebabkan nyawa manusia terancam, kerusakan bangunan dan ekologi sekitarnya. Kebakaran dapat menjadi permasalahan yang serius jika tidak ditangani secara cepat karena akan menimbulkan kerugian yang sangat besar. Oleh karena itu, peran pemadam kebakaran sangat penting. Pemadam Kebakaran harus siap siaga apabila terjadi kebakaran dan dituntut harus memilih rute perjalanan yang optimal untuk datang ke tempat kejadian dengan cepat. Masalah pencarian rute perjalanan ini terkadang menjadi sebuah permasalahan yang cukup kompleks. Hal ini disebabkan kondisi jalan yang harus dilewati mobil pemadam kebakaran memiliki tingkat kepadatan kendaraan yang cukup tinggi. Pada penelitian tugas akhir ini, penulis membuat sebuah aplikasi pencarian rute untuk mobil pemadam kebakaran berbasis android dengan menggunakan algoritma Floyd-Warshall. 

## Input

        Tujuan dari penelitian ini antara lain :
        (1) Membuat sebuah aplikasi yang dapat memberikan informasi kepada pengemudi mobil pemadam kebakaran tentang pencarian dan penentuan rute optimal ke tempat tujuan.
        (2) Merancang dan mengimplementasikan metode Simple Additive Weighting (SAW) ke dalam sebuah aplikasi berbasis android untuk memperhitungkan kriteria jarak, nilai kemacetan dan posisi hydrant dalam menentukan bobot suatu jalan ?
        (3) Bagaimana merancang dan mengimplementasikan algoritma Floyd-Warshall ke dalam aplikasi pencarian rute optimal untuk pemadam kebakaran berbasis smartphone Android ?

## Proses
        
        Pengujian Metode dan Algoritma
        Pengujian Metode SAW
        Pengujian Algoritma Floyd-Warshall
        Pengujian Pencarian Rute

## output

        Dari hasil pengujian dan analisis yang telah dilakukan maka dapat disimpulkan menjadi beberapa hal sebagai berikut :
        1. Metode Simple Additive Weighting (SAW) dan Algoritma Floyd-Warshall dapat dirancang dan diimplementasikan ke dalam sebuah aplikasi berbasis android untuk pencarian rute optimal.
        2. Berdasarkan hasil pengujian metode dan algoritma terhadap 3 rute pada skenario uji, dihasilkan hasil yang sama antara perhitungan manual dan hasil keluaran yang dihasilkan oleh aplikasi.
        3. Algoritma Floyd-Warshall dapat diterapkan untuk proses pencarian rute, akan tetapi tidak akan cukup efektif apabila diterapkan untuk proses pencarian rute dengan cakupan luas daerah yang cukup besar, karena hasil yang disarankan tidak akan cukup optimal.
