/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MapGenerator;

import org.jxmapviewer.viewer.DefaultWaypoint;
import org.jxmapviewer.viewer.GeoPosition;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class SwingWayPoint extends DefaultWaypoint {
    private final JButton button;
    private final String text;

    public SwingWayPoint(String text, GeoPosition coord) {
        super(coord);
        this.text = text;
        ImageIcon cc = new ImageIcon("src\\main\\java\\menu\\fire.gif");
        button = new JButton(cc);
        button.setSize(30, 20);
        button.setPreferredSize(new Dimension(32, 32));
        button.setBorderPainted(false);
        button.addMouseListener(new SwingWaypointMouseListener());
        button.setVisible(true);
    }
    
    public SwingWayPoint(GeoPosition coord) {
        super(coord);
        this.text = "water";
        ImageIcon cc = new ImageIcon("src\\main\\java\\menu\\icons8_water_30px_1.png");
        button = new JButton(cc);
        button.setSize(30, 20);
        button.setPreferredSize(new Dimension(32, 32));
        button.setBorderPainted(false);
        button.addMouseListener(new SwingWaypointMouseListener());
        button.setVisible(true);
    }

    JButton getButton() {
        return button;
    }

    private class SwingWaypointMouseListener implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            JOptionPane.showMessageDialog(button, "You clicked on " + text);
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    }
}
