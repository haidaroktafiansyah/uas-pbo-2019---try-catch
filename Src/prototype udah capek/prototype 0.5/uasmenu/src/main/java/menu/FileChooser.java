package menu;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FileChooser {

    static String fileDictName = "";

    public void filechooserku () throws FileNotFoundException, IOException {
        XSSFWorkbook workbook;

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Open the excel file"); //name for chooser
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel Files", "csv"); //filter to show only that
        fileChooser.setAcceptAllFileFilterUsed(false); //to show or not all other files
        fileChooser.addChoosableFileFilter(filter);
        fileChooser.setSelectedFile(new File(fileDictName)); //when you want to show the name of file into the chooser
        fileChooser.setVisible(true);
        int result = fileChooser.showOpenDialog(fileChooser);
        if (result == JFileChooser.APPROVE_OPTION) {
            fileDictName = fileChooser.getSelectedFile().getAbsolutePath();
        } else {
            return;
        }

        File file = new File(fileDictName);
        if (file.exists() == false) {
            workbook = new XSSFWorkbook();
            XSSFSheet exampleSheet = workbook.createSheet("1");
            XSSFRow firstRow = exampleSheet.createRow(1);
            XSSFCell cell = firstRow.createCell(0);
            cell.setCellValue("value");

            try (
                    //Write the workbook in file system
                     FileOutputStream out = new FileOutputStream(file)) {
                workbook.write(out);
            }
        } else {
            // Sheet already exists
            //System.out.println(fileChooser.getSelectedFile());
            ReadExcel read = new ReadExcel();
            ReadCsv read2 = new ReadCsv();
            Jlist read3 = new Jlist();
            //read.readexcelku(fileChooser.getSelectedFile());
            //read2.readcsvku(fileChooser.getSelectedFile());
            read3.readcsvku(fileChooser.getSelectedFile());
            read3.tampilJlist();
        }
    }

}
