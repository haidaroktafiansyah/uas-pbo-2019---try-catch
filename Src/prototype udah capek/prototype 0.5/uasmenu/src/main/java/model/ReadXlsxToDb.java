package model;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class ReadXlsxToDb {

    public static void readcsvku(File excel) throws IOException, InvalidFormatException {

        // Creating a Workbook from an Excel file (.xls or .xlsx)
        FileInputStream file = new FileInputStream(excel);
        Workbook workbook = WorkbookFactory.create((file));

        // Retrieving the number of sheets in the Workbook
        System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

        Sheet sheet = workbook.getSheetAt(0);

        Iterator<Row> iterator = sheet.iterator();
        Row getSchool = sheet.getRow(4);

        Iterator<Cell> cellIterator = getSchool.cellIterator();
        
        DBHelper dbku = new DBHelper();
        
        for (Row row : sheet) {
            int i = 0;
            Cell tempcell = null;
            for (Cell cell : row) {
                if (i == 4 || i == 5) {
                    if (i == 4) {
                        tempcell = cell;
                    } else {
                        String SQL = "INSERT INTO kordinat "
                                + "(x,y)VALUES ('"+cell+"','"+tempcell+"');";
                        
                        //System.out.println(SQL);
                        dbku.executeQuery(SQL);
                    }
                }
                i++;
            }
            System.out.print("\n");
        }

        // Closing the workbook
        workbook.close();
    }
}
