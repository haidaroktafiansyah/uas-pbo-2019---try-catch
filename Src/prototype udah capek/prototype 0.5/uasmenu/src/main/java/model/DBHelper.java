/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author mwpras
 */
import java.sql.*;
        
public class DBHelper {
    public static Connection koneksi;

    private static String namedb = "uaspbo";
    private static String userdb = "root";
    private static String passworddb = "";

    public DBHelper(String namedb, String userdb, String passworddb) {
        this.namedb = namedb;
        this.userdb = userdb;
        this.passworddb = passworddb;
    }
    
    
    public DBHelper() {
    }
    
    
    public static void bukaKoneksi(){
        if(koneksi == null){
            try{
                String url = "jdbc:mysql://localhost/"+namedb;
                String user = userdb;
                String password = passworddb;
                //DriverManager.registerDriver(new com.mysql.jdbc.Driver());
                koneksi = DriverManager.getConnection(url, user, password);
            }
            catch (SQLException t){
                System.out.println("Error koneksi!");
            }
        }
    }
    
    
    public static boolean executeQuery(String query){
        bukaKoneksi();
        boolean result = false;
        
        try{
            Statement stmt = koneksi.createStatement();
            stmt.executeUpdate(query);
            
            result = true;
            
            stmt.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
}
