/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import javax.swing.JFrame;
import javax.swing.JRootPane;

/**
 * JList basic tutorial and example
 *
 * @author wwww.codejava.net
 */
public class Jlist extends JFrame {

    //simpan untuk tampilan JList
    private JList<String> countryList;
    //temp Jlist
    DefaultListModel<String> listModel = new DefaultListModel<>();
    //simpan lintang dan bujur
    ArrayList<ArrayList<String>> SimpanDir = new ArrayList<>(500);

    public void readcsvku(File inputcsv) throws FileNotFoundException, IOException {

        FileInputStream file = new FileInputStream(inputcsv);

        //this holds the Csv file location
        //String csvFile = "H:/CSVFiles/csvfile.csv";
        String line = "";
        //as we have to split the file from commas
        String splitBy = ",";
        int i = 0;

        //Buffered reader class is a java.io class which reads 
        //a character input file ,it reads lines and arrays 
        //File reader opens the given file in read mode 
        BufferedReader br = new BufferedReader(new FileReader(inputcsv)); //readline function is use to read a line form the file
        while ((line = br.readLine()) != null) {
            //loop will continue until the line ends 
            String[] name = line.split(splitBy);
            //split function use to split the words in the line by commas
            //System.out.println("Lintang: " + name[0] + " , Bujur:" + name[1]);
            //this is to print the each csv line 
            SimpanDir.add(new ArrayList());
            SimpanDir.get(i).add(name[0]);
            SimpanDir.get(i).add(name[1]);
            i++;
            //create the model and add elements
            listModel.addElement(name[0] + "\t" + name[1] + "\t" + name[6] + "\t" + name[7] + "\t" + name[8]);
        }
        //test output arraylist
        //System.out.println(SimpanDir.get(1).get(0)+"   "+SimpanDir.get(1).get(1));
    }

    public void tampilJlist() {

        //create the list
        countryList = new JList<>(listModel);
        countryList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    System.out.println(countryList.getSelectedValuesList());
                    int index = listModel.indexOf(countryList.getSelectedValue());
                    System.out.println("on index "+index);
                    GoogleMapsSample google = new GoogleMapsSample();
                    google.PanggilMap(SimpanDir.get(index).get(0),SimpanDir.get(index).get(1));
                }
            }
        });

        add(new JScrollPane(countryList));

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Pilih Koordinat");
        this.setSize(600, 1000);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
