/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MapGenerator;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.event.MouseInputListener;

import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.OSMTileFactoryInfo;
import org.jxmapviewer.cache.FileBasedLocalCache;
import org.jxmapviewer.input.CenterMapListener;
import org.jxmapviewer.input.PanKeyListener;
import org.jxmapviewer.input.PanMouseInputListener;
import org.jxmapviewer.input.ZoomMouseWheelListenerCenter;
import org.jxmapviewer.viewer.DefaultTileFactory;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.TileFactoryInfo;
import org.jxmapviewer.viewer.WaypointPainter;

/**
 * A sample application demonstrating usage of Swing components as waypoints
 * using JXMapViewer.
 *
 * @author Daniel Stahr
 */
public class Map {
    public static void Mapku(String a, String b) {
        // Create a TileFactoryInfo for OSM
        TileFactoryInfo info = new OSMTileFactoryInfo();
        DefaultTileFactory tileFactory = new DefaultTileFactory(info);
        tileFactory.setThreadPoolSize(8);

        // Setup local file cache
        File cacheDir = new File(System.getProperty("user.home") + File.separator + ".jxmapviewer2");
        tileFactory.setLocalCache(new FileBasedLocalCache(cacheDir, false));

        // Setup JXMapViewer
        JXMapViewer mapViewer = new JXMapViewer();
        mapViewer.setTileFactory(tileFactory);

        GeoPosition Fire = new GeoPosition(Double.parseDouble(a), Double.parseDouble(b));
        //GeoPosition Water = new GeoPosition(-0.855172, 102.842);
//        GeoPosition mainz     = new GeoPosition(50,  0, 0, 8, 16, 0);
//        GeoPosition darmstadt = new GeoPosition(49, 52, 0, 8, 39, 0);
//        GeoPosition offenbach = new GeoPosition(50,  6, 0, 8, 46, 0);

        // Set the focus
        mapViewer.setZoom(10);
        mapViewer.setAddressLocation(Fire);

        // Add interactions
        MouseInputListener mia = new PanMouseInputListener(mapViewer);
        mapViewer.addMouseListener(mia);
        mapViewer.addMouseMotionListener(mia);
        mapViewer.addMouseListener(new CenterMapListener(mapViewer));
        mapViewer.addMouseWheelListener(new ZoomMouseWheelListenerCenter(mapViewer));
        mapViewer.addKeyListener(new PanKeyListener(mapViewer));

        // Create waypoints from the geo-positions
        Set<SwingWayPoint> waypoints = new HashSet<SwingWayPoint>(Arrays.asList(
                new SwingWayPoint("Fire", Fire)
                //,
                //new SwingWayPoint("Water", Water)
//                ,
//                new SwingWayPoint("Mainz", mainz),
//                new SwingWayPoint("Darmstadt", darmstadt),
//                new SwingWayPoint("Offenbach", offenbach)
                ));

        // Set the overlay painter
        WaypointPainter<SwingWayPoint> swingWaypointPainter = new SwingWaypointOverlayPainter();
        swingWaypointPainter.setWaypoints(waypoints);
        mapViewer.setOverlayPainter(swingWaypointPainter);

        // Add the JButtons to the map viewer
        for (SwingWayPoint w : waypoints) {
            mapViewer.add(w.getButton());
        }

        // Display the viewer in a JFrame
        JFrame frame = new JFrame("JXMapviewer2 Example 7");
        frame.getContentPane().add(mapViewer);
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}