/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReadCsv {

    public void readcsvku(File inputcsv) throws FileNotFoundException, IOException {

        FileInputStream file = new FileInputStream(inputcsv);

        //this holds the Csv file location
        //String csvFile = "H:/CSVFiles/csvfile.csv";
        String line = "";
        //as we have to split the file from commas
        String splitBy = ",";
        ArrayList<ArrayList<String>> SimpanDir = new ArrayList<>(500);
        int i = 0;

        //Buffered reader class is a java.io class which reads 
        //a character input file ,it reads lines and arrays 
        //File reader opens the given file in read mode 
        BufferedReader br = new BufferedReader(new FileReader(inputcsv)); //readline function is use to read a line form the file
        while ((line = br.readLine()) != null) {
            //loop will continue until the line ends 
            String[] name = line.split(splitBy);
            //split function use to split the words in the line by commas
            //System.out.println("Lintang: " + name[0] + " , Bujur:" + name[1]);
            //this is to print the each csv line 
            SimpanDir.add(new ArrayList());
            SimpanDir.get(i).add(name[0]);
            SimpanDir.get(i).add(name[1]);
            i++;
        }
        //test output arraylist
        //System.out.println(SimpanDir.get(1).get(0)+"   "+SimpanDir.get(1).get(1));
    }
}
